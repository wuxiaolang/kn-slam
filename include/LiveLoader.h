#ifndef LIVELOADER_H
#define LIVELOADER_H

#include "Loader.h"

namespace slam
{

class LiveLoader : public Loader
{
  public:
  LiveLoader(int index, const std::shared_ptr<slam::System> &system);
  void run();

  private:
  int mCameraIndex;
};
}

#endif
