#ifndef LOADER_H
#define LOADER_H

#include <memory>
#include <mutex>
#include <string>

#include "System.h"

namespace slam
{
class Viewer;
class Loader
{
  public:  
  Loader(const std::shared_ptr<slam::System> &system)
    : mpSystem(system)
    , mpViewer(nullptr)
    , mStop(false)
  {
  }

  void run()
  {
  }

  void stop()
  {
    std::unique_lock<std::mutex> lock(mMutexStop);
    mStop = true;
  }

  void setViewer(Viewer *viewer)
  {
    mpViewer = viewer;
  }

protected:
  bool stopRequested()
  {
    std::unique_lock<std::mutex> lock(mMutexStop);
    return mStop;
  }

  std::shared_ptr<slam::System> mpSystem;
  Viewer *mpViewer;

  bool mStop;
  std::mutex mMutexStop;
};
}

#endif
