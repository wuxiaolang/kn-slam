/*
 * performance_monitor.h
 *
 *  Created on: Aug 26, 2011
 *      Author: Christian Forster
 */

#ifndef PERFORMANCE_MONITOR_H
#define PERFORMANCE_MONITOR_H

#include <Timer.h>
#include <fstream>
#include <iostream>
#include <map>
#include <string>

namespace slam
{

struct LogItem {
  double data;
  bool set;
};

class PerformanceMonitor
{
  public:
  PerformanceMonitor();
  ~PerformanceMonitor();
  void init(const std::string &trace_name, const std::string &trace_dir);
  void addTimer(const std::string &name);
  void addLog(const std::string &name);
  void writeToFile();
  void startTimer(const std::string &name);
  void stopTimer(const std::string &name);
  double getTime(const std::string &name) const;
  void printTime(const std::string &name);
  void log(const std::string &name, double data);

  private:
  std::map<std::string, Timer> timers_;
  std::map<std::string, LogItem> logs_;
  std::string trace_name_; //<! name of the thread that started the performance monitor
  std::string trace_dir_;  //<! directory where the logfiles are saved
  std::ofstream ofs_;

  void trace();
  void traceHeader();
};

} // namespace slam

#endif // PERFORMANCE_MONITOR_H
