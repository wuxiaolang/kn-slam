#ifndef SETTINGS_H
#define SETTINGS_H

#include <opencv2/core/persistence.hpp>
#include <string>

namespace slam
{

class Settings
{
  public:
  static Settings &getInstance();
  void loadFrom(const std::string &fileName);
  cv::FileNode get(const std::string &nodeName);
  cv::FileNode operator[](const std::string &nodename);

  private:
  Settings();
  ~Settings();

  cv::FileStorage mSettings;
};
}

#endif // SETTINGS_H
