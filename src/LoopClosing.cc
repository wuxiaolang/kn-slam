/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <Converter.h>
#include <LoopClosing.h>
#include <ORBmatcher.h>
#include <Optimizer.h>
#include <Sim3Solver.h>
#include <global.h>

#include <mutex>
#include <thread>

namespace slam
{

LoopClosing::LoopClosing(Map *pMap, KeyFrameDatabase *pDB, ORBVocabulary *pVoc, const bool bFixScale)
    : mbResetRequested(false)
    , mbFinishRequested(false)
    , mbFinished(true)
    , mpMap(pMap)
    , mpKeyFrameDB(pDB)
    , mpORBVocabulary(pVoc)
    , mpMatchedKF(NULL)
    , mLastLoopKFid(0)
    , mbRunningGBA(false)
    , mbFinishedGBA(true)
    , mbStopGBA(false)
    , mpThreadGBA(NULL)
    , mbFixScale(bFixScale)
    , mnFullBAIdx(0)
{
  mnCovisibilityConsistencyTh = 3;
}

void LoopClosing::SetTracker(Tracking *pTracker)
{
  mpTracker = pTracker;
}

void LoopClosing::SetLocalMapper(LocalMapping *pLocalMapper)
{
  mpLocalMapper = pLocalMapper;
}


void LoopClosing::Run()
{
  mbFinished = false;

  while (1) {
    // Check if there are keyframes in the queue
    if (CheckNewKeyFrames()) {
      // Detect loop candidates and check covisibility consistency
      if (DetectLoop()) {
        // Compute similarity transformation [sR|t] (s=1 in stereo/rgbd)
        if (ComputeSim3()) {
          // Perform loop fusion and pose graph optimization
          CorrectLoop();
        }
      }
    }

    ResetIfRequested();

    if (CheckFinish())
      break;

    usleep(5000);
  }

  SetFinish();
}

void LoopClosing::InsertKeyFrame(KeyFrame *pKF)
{
  std::unique_lock<std::mutex> lock(mMutexLoopQueue);
  if (pKF->mnId != 0)
    mlpLoopKeyFrameQueue.push_back(pKF);
}

bool LoopClosing::CheckNewKeyFrames()
{
  std::unique_lock<std::mutex> lock(mMutexLoopQueue);
  return (!mlpLoopKeyFrameQueue.empty());
}

bool LoopClosing::DetectLoop()
{
  {
    std::unique_lock<std::mutex> lock(mMutexLoopQueue);
    mpCurrentKF = mlpLoopKeyFrameQueue.front();
    mlpLoopKeyFrameQueue.pop_front();
    // Avoid that a keyframe can be erased while it is being process by this thread
    mpCurrentKF->SetNotErase();
  }

  // Less than 10 KF since last loop detection
  if (mpCurrentKF->mnId < mLastLoopKFid + 10) {
    mpKeyFrameDB->add(mpCurrentKF);
    mpCurrentKF->SetErase();
    return false;
  }

  // Compute reference BoW similarity score
  // This is the lowest score to a connected keyframe in the covisibility graph
  // We will impose loop candidates to have a higher similarity than this
  const DBoW2::BowVector &CurrentBowVec = mpCurrentKF->mBowVec;
  float minScore = 1;
  for (KeyFrame *pKF : mpCurrentKF->GetCovisibleKeyFrames()) {
    if (pKF->isBad())
      continue;

    const DBoW2::BowVector &BowVec = pKF->mBowVec;
    float score = mpORBVocabulary->score(CurrentBowVec, BowVec);
    if (score < minScore)
      minScore = score;
  }

  // Query the database imposing the minimum score
  std::vector<KeyFrame *> vpCandidateKFs = mpKeyFrameDB->DetectLoopCandidates(mpCurrentKF, minScore);
  if (vpCandidateKFs.empty()) {
    mpKeyFrameDB->add(mpCurrentKF);
    mvConsistentGroups.clear();
    mpCurrentKF->SetErase();
    return false;
  }

  // For each loop candidate check consistency with previous loop candidates
  // Each candidate expands into a covisibility group
  // A group is consistent with a previous group if they share at least a keyframe
  // We must detect a consistent loop in several consecutive keyframes to accept it
  mvpEnoughConsistentCandidates.clear();

  std::vector<ConsistentGroup> vCurrentConsistentGroups;
  std::vector<bool> vbConsistentGroup(mvConsistentGroups.size(), false);
  for (KeyFrame *pCandidateKF : vpCandidateKFs) {
    std::set<KeyFrame *> spCandidateGroup = pCandidateKF->GetConnectedKeyFrames();
    spCandidateGroup.insert(pCandidateKF);

    bool bEnoughConsistent = false;
    bool bConsistentForSomeGroup = false;
    for (size_t iG = 0, iendG = mvConsistentGroups.size(); iG < iendG; iG++) {
      std::set<KeyFrame *> sPreviousGroup = mvConsistentGroups[iG].first;

      bool bConsistent = false;
      for (auto spCandidate : spCandidateGroup) {
        if (sPreviousGroup.count(spCandidate)) {
          bConsistent = true;
          bConsistentForSomeGroup = true;
          break;
        }
      }

      if (bConsistent) {
        int nPreviousConsistency = mvConsistentGroups[iG].second;
        int nCurrentConsistency = nPreviousConsistency + 1;
        if (!vbConsistentGroup[iG]) {
          ConsistentGroup cg = std::make_pair(spCandidateGroup, nCurrentConsistency);
          vCurrentConsistentGroups.push_back(cg);
          vbConsistentGroup[iG] = true; //this avoid to include the same group more than once
        }
        if (nCurrentConsistency >= mnCovisibilityConsistencyTh && !bEnoughConsistent) {
          mvpEnoughConsistentCandidates.push_back(pCandidateKF);
          bEnoughConsistent = true; //this avoid to insert the same candidate more than once
        }
      }
    }

    // If the group is not consistent with any previous group insert with consistency counter set to zero
    if (!bConsistentForSomeGroup) {
      ConsistentGroup cg = std::make_pair(spCandidateGroup, 0);
      vCurrentConsistentGroups.push_back(cg);
    }
  }

  // Update Covisibility Consistent Groups
  mvConsistentGroups = vCurrentConsistentGroups;

  // Add Current Keyframe to database
  mpKeyFrameDB->add(mpCurrentKF);

  if (mvpEnoughConsistentCandidates.empty()) {
    mpCurrentKF->SetErase();
    return false;
  } else {
    return true;
  }
}

bool LoopClosing::ComputeSim3()
{
  // For each consistent loop candidate we try to compute a Sim3
  const int nInitialCandidates = mvpEnoughConsistentCandidates.size();

  // We compute first ORB matches for each candidate
  // If enough matches are found, we setup a Sim3Solver
  ORBmatcher matcher(0.75, true);

  std::vector<Sim3Solver *> vpSim3Solvers;
  vpSim3Solvers.resize(nInitialCandidates);

  std::vector<std::vector<MapPoint *>> vvpMapPointMatches;
  vvpMapPointMatches.resize(nInitialCandidates);

  std::vector<bool> vbDiscarded;
  vbDiscarded.resize(nInitialCandidates);

  int nCandidates = 0; //candidates with enough matches

  for (int i = 0; i < nInitialCandidates; i++) {
    KeyFrame *pKF = mvpEnoughConsistentCandidates[i];

    // avoid that local mapping erase the KF while it is being processed in this thread
    pKF->SetNotErase();

    if (pKF->isBad()) {
      vbDiscarded[i] = true;
      continue;
    }

    // Search for enough ORB matches between the last keyframe and the current candidate
    int nmatches = matcher.SearchByBoW(mpCurrentKF, pKF, vvpMapPointMatches[i]);
    if (nmatches < 15) { // @fradelg: 20
      vbDiscarded[i] = true;
      continue;
    } else {
      if (nmatches < 20) {
        // WARNING: do not use DEBUG_STREAM here without the curly braces!!
        // It will make the statement dissappear if SLAM_DEBUG is not defined
        // So the next statement (usually off the condition) will be included inside "if"
        WARN_STREAM("LOOP", "Weak candidate with only " << nmatches << " ORB matches")
      }
      vpSim3Solvers[i] = new Sim3Solver(mpCurrentKF, pKF, vvpMapPointMatches[i], mbFixScale);
      vpSim3Solvers[i]->SetRansacParameters(0.99, 10, 300); // @fradelg: 0.99, 20, 300
    }

    nCandidates++;
  }

  bool bMatch = false;

  // Perform alternatively RANSAC iterations for each candidate until one is succesful
  while (nCandidates > 0 && !bMatch) {
    for (int i = 0; i < nInitialCandidates; i++) {
      if (vbDiscarded[i])
        continue;

      // Perform 5 RANSAC iterations
      std::vector<bool> vbInliers;
      int nInliers;
      bool bNoMore;
      Sim3Solver *pSolver = vpSim3Solvers[i];
      cv::Mat Scm = pSolver->iterate(5, bNoMore, vbInliers, nInliers);

      // If Ransac reachs max iterations discard keyframe
      if (bNoMore) {
        DEBUG_STREAM("LOOP", "RANSAC solver reach max iterations: " << pSolver->GetMaxIterations())
        vbDiscarded[i] = true;
        nCandidates--;
      }

      // Perform a guided matching and optimize with all correspondences for this Sim3
      if (!Scm.empty()) {
        std::vector<MapPoint *> vpMapPointMatches(vvpMapPointMatches[i].size(), static_cast<MapPoint *>(NULL));
        for (size_t j = 0, jend = vbInliers.size(); j < jend; j++) {
          if (vbInliers[j])
            vpMapPointMatches[j] = vvpMapPointMatches[i][j];
        }

        cv::Mat R = pSolver->GetEstimatedRotation();
        cv::Mat t = pSolver->GetEstimatedTranslation();
        const float s = pSolver->GetEstimatedScale();

        KeyFrame *pKF = mvpEnoughConsistentCandidates[i];
        const int nFound = matcher.SearchBySim3(mpCurrentKF, pKF, vpMapPointMatches, s, R, t, 8); // @fradelg: 7.5

        g2o::Sim3 gScm(Converter::toMatrix3d(R), Converter::toVector3d(t), s);
        const int nInliers = Optimizer::OptimizeSim3(mpCurrentKF, pKF, vpMapPointMatches, gScm, 10, mbFixScale);

        // Use a smarter min of inliers (at least half of map points must verify the model)
        const int nMinInliers = std::max(10, nFound / 2);

        // If optimization is succesful stop RANSAC and continue
        if (nInliers >= nMinInliers) { // @fradelg: 20
          bMatch = true;
          mpMatchedKF = pKF;
          g2o::Sim3 gSmw(Converter::toMatrix3d(pKF->GetRotation()), Converter::toVector3d(pKF->GetTranslation()), 1.0);
          mg2oScw = gScm * gSmw;
          mScw = Converter::toCvMat(mg2oScw);
          mvpCurrentMatchedPoints = vpMapPointMatches;
          DEBUG_STREAM("LOOP", "Sim3 supported by " << nInliers << " / " << nMinInliers)
          break;
        } else {
          DEBUG_STREAM("LOOP", "Sim3 has not enough inliers: " << nInliers << " / " << nMinInliers)
        }
      }
    }
  }

  if (!bMatch) {
    for (int i = 0; i < nInitialCandidates; i++)
      mvpEnoughConsistentCandidates[i]->SetErase();
    mpCurrentKF->SetErase();
    return false;
  }

  // Retrieve all MapPoints seen in Loop Keyframe and its neighbors
  std::vector<KeyFrame *> vpLoopConnectedKFs = mpMatchedKF->GetCovisibleKeyFrames();
  vpLoopConnectedKFs.push_back(mpMatchedKF);
  mvpLoopMapPoints.clear();
  for (KeyFrame *pKF : vpLoopConnectedKFs) {
    for (MapPoint *pMP : pKF->GetMapPointMatches()) {
      if (pMP) {
        if (!pMP->isBad() && pMP->mnLoopPointForKF != mpCurrentKF->mnId) {
          mvpLoopMapPoints.push_back(pMP);
          pMP->mnLoopPointForKF = mpCurrentKF->mnId;
        }
      }
    }
  }

  // Find more 2D-3D matches projecting with the computed Sim3
  matcher.SearchByProjection(mpCurrentKF, mScw, mvpLoopMapPoints, mvpCurrentMatchedPoints, 10);

  // Count the number of points (including both Sim3 matches and previous)
  int nTotalMatches = std::count_if(mvpCurrentMatchedPoints.begin(), mvpCurrentMatchedPoints.end(),
      [](MapPoint *pMP) { return pMP; });

  // Accept this loop if enough point matches are detected
  if (nTotalMatches >= 50) { // @fradelg: 40
    INFO_STREAM("LOOP", "Accepted. Supported by MapPoints: " << nTotalMatches)
    for (KeyFrame *pKF : mvpEnoughConsistentCandidates)
      if (pKF != mpMatchedKF)
        pKF->SetErase();
    return true;
  } else {
    DEBUG_STREAM("LOOP", "Rejected. Supported by MapPoints: " << nTotalMatches)
    for (KeyFrame *pKF : mvpEnoughConsistentCandidates)
      pKF->SetErase();
    mpCurrentKF->SetErase();
    return false;
  }
}

void LoopClosing::CorrectLoop()
{
  // Send a stop signal to Local Mapping
  // Avoid new keyframes while correcting the loop
  mpLocalMapper->RequestStop();

  // If a Global Bundle Adjustment is running, abort it
  if (isRunningGBA()) {
    std::unique_lock<std::mutex> lock(mMutexGBA);
    mbStopGBA = true;

    mnFullBAIdx++;

    if (mpThreadGBA) {
      mpThreadGBA->detach();
      delete mpThreadGBA;
    }
  }

  // Wait until Local Mapping has effectively stopped
  while (!mpLocalMapper->isStopped()) {
    usleep(1000);
  }

  // Update current keyframe
  mpCurrentKF->UpdateConnections();

  // Retrive keyframes connected to the current keyframe and compute corrected Sim3 pose by propagation
  mvpCurrentConnectedKFs = mpCurrentKF->GetCovisibleKeyFrames();
  mvpCurrentConnectedKFs.push_back(mpCurrentKF);

  KeyFrameAndPose CorrectedSim3, NonCorrectedSim3;
  CorrectedSim3[mpCurrentKF] = mg2oScw;
  cv::Mat Twc = mpCurrentKF->GetPoseInverse();

  {
    // Get Map Mutex
    std::unique_lock<std::mutex> lock(mpMap->mMutexMapUpdate);

    // Compute corrected poses with the Sim3 of the loop closure
    for (KeyFrame *pKFi : mvpCurrentConnectedKFs) {
      cv::Mat Tiw = pKFi->GetPose();

      if (pKFi != mpCurrentKF) {
        cv::Mat Tic = Tiw * Twc;
        cv::Mat Ric = Tic.rowRange(0, 3).colRange(0, 3);
        cv::Mat tic = Tic.rowRange(0, 3).col(3);
        g2o::Sim3 g2oSic(Converter::toMatrix3d(Ric), Converter::toVector3d(tic), 1.0);
        g2o::Sim3 g2oCorrectedSiw = g2oSic * mg2oScw;
        CorrectedSim3[pKFi] = g2oCorrectedSiw;
      }

      cv::Mat Riw = Tiw.rowRange(0, 3).colRange(0, 3);
      cv::Mat tiw = Tiw.rowRange(0, 3).col(3);
      g2o::Sim3 g2oSiw(Converter::toMatrix3d(Riw), Converter::toVector3d(tiw), 1.0);
      NonCorrectedSim3[pKFi] = g2oSiw;
    }

    // Correct all MapPoints observed by current keyframe and neighbors, 
    // so that they align with the other side of the loop
    for (auto &kfp : CorrectedSim3) {
      KeyFrame *pKFi = kfp.first;
      g2o::Sim3 g2oCorrectedSiw = kfp.second;
      g2o::Sim3 g2oCorrectedSwi = g2oCorrectedSiw.inverse();
      g2o::Sim3 g2oSiw = NonCorrectedSim3[pKFi];

      for (MapPoint *pMPi : pKFi->GetMapPointMatches()) {

        if (!pMPi)
          continue;
        if (pMPi->isBad())
          continue;
        if (pMPi->mnCorrectedByKF == mpCurrentKF->mnId)
          continue;

        // Apply non-corrected and then corrected pose
        cv::Mat P3Dw = pMPi->GetWorldPos();
        Eigen::Vector3d eigP3Dw = Converter::toVector3d(P3Dw);
        Eigen::Vector3d eigCorrectedP3Dw = g2oCorrectedSwi.map(g2oSiw.map(eigP3Dw));

        cv::Mat cvCorrectedP3Dw = Converter::toCvMat(eigCorrectedP3Dw);
        pMPi->SetWorldPos(cvCorrectedP3Dw);
        pMPi->mnCorrectedByKF = mpCurrentKF->mnId;
        pMPi->mnCorrectedReference = pKFi->mnId;
        pMPi->UpdateNormalAndDepth();
      }

      // Update keyframe pose with corrected Sim3. First transform Sim3 to SE3 [R t/s;0 1]
      Eigen::Matrix3d eigR = g2oCorrectedSiw.rotation().toRotationMatrix();
      Eigen::Vector3d eigt = g2oCorrectedSiw.translation() * (1.0 / g2oCorrectedSiw.scale());
      pKFi->SetPose(Converter::toCvSE3(eigR, eigt));

      // Make sure connections are updated
      pKFi->UpdateConnections();
    }

    // Start Loop Fusion
    // Update matched map points and replace if duplicated
    for (size_t i = 0; i < mvpCurrentMatchedPoints.size(); i++) {
      if (mvpCurrentMatchedPoints[i]) {
        MapPoint *pLoopMP = mvpCurrentMatchedPoints[i];
        MapPoint *pCurMP = mpCurrentKF->GetMapPoint(i);
        if (pCurMP)
          pCurMP->Replace(pLoopMP);
        else {
          mpCurrentKF->AddMapPoint(pLoopMP, i);
          pLoopMP->AddObservation(mpCurrentKF, i);
          pLoopMP->ComputeDistinctiveDescriptors();
        }
      }
    }
  }

  // Project MapPoints observed in the neighborhood of the loop keyframe
  // into the current keyframe and neighbors using corrected poses.
  // Then, fuse duplicated points
  SearchAndFuse(CorrectedSim3);

  // After the MapPoint fusion, new links in the covisibility graph will appear attaching both sides of the loop
  std::map<KeyFrame *, std::set<KeyFrame *>> LoopConnections;
  for (KeyFrame *pKFi : mvpCurrentConnectedKFs) {
    std::vector<KeyFrame *> vpPreviousNeighbors = pKFi->GetCovisibleKeyFrames();

    // Update connections and detect new links
    pKFi->UpdateConnections();
    LoopConnections[pKFi] = pKFi->GetConnectedKeyFrames();

    for (KeyFrame *pKFe : vpPreviousNeighbors)
      LoopConnections[pKFi].erase(pKFe);

    for (KeyFrame *pKFe : mvpCurrentConnectedKFs)
      LoopConnections[pKFi].erase(pKFe);
  }

  // Optimize graph
  Optimizer::OptimizeEssentialGraph(mpMap, mpMatchedKF, mpCurrentKF, NonCorrectedSim3, CorrectedSim3, LoopConnections, mbFixScale);

  mpMap->InformNewBigChange();

  // Add loop edge
  mpMatchedKF->AddLoopEdge(mpCurrentKF);
  mpCurrentKF->AddLoopEdge(mpMatchedKF);

  // Launch a new thread to perform Global Bundle Adjustment
  mbRunningGBA = true;
  mbFinishedGBA = false;
  mbStopGBA = false;
  mpThreadGBA = new std::thread(&LoopClosing::RunGlobalBundleAdjustment, this, mpCurrentKF->mnId);

  // Loop closed. Release Local Mapping.
  mpLocalMapper->Release();

  mLastLoopKFid = mpCurrentKF->mnId;
}

void LoopClosing::SearchAndFuse(const KeyFrameAndPose &CorrectedPosesMap)
{
  ORBmatcher matcher(0.8f);

  for (auto &kfp : CorrectedPosesMap) {
    KeyFrame *pKF = kfp.first;
    g2o::Sim3 g2oScw = kfp.second;
    cv::Mat cvScw = Converter::toCvMat(g2oScw);
    std::vector<MapPoint *> vpReplacePoints(mvpLoopMapPoints.size(), static_cast<MapPoint *>(NULL));
    matcher.Fuse(pKF, cvScw, mvpLoopMapPoints, 4, vpReplacePoints);

    // Get Map Mutex
    std::unique_lock<std::mutex> lock(mpMap->mMutexMapUpdate);
    for (size_t i = 0; i < mvpLoopMapPoints.size(); i++) {
      MapPoint *pRep = vpReplacePoints[i];
      if (pRep) {
        pRep->Replace(mvpLoopMapPoints[i]);
      }
    }
  }
}


void LoopClosing::RequestReset()
{
  {
    std::unique_lock<std::mutex> lock(mMutexReset);
    mbResetRequested = true;
  }

  while (1) {
    {
      std::unique_lock<std::mutex> lock2(mMutexReset);
      if (!mbResetRequested)
        break;
    }
    usleep(5000);
  }
}

void LoopClosing::ResetIfRequested()
{
  std::unique_lock<std::mutex> lock(mMutexReset);
  if (mbResetRequested) {
    mlpLoopKeyFrameQueue.clear();
    mLastLoopKFid = 0;
    mbResetRequested = false;
  }
}

void LoopClosing::RunGlobalBundleAdjustment(unsigned long nLoopKF)
{
  size_t idx = mnFullBAIdx;
  const uint32_t nIterations = 20; // @fradelg: 10
  Optimizer::GlobalBundleAdjustment(mpMap, nIterations, &mbStopGBA, nLoopKF, false);

  // Update all MapPoints and KeyFrames
  // Local Mapping was active during BA, that means that there might be new keyframes
  // not included in the Global BA and they are not consistent with the updated map.
  // We need to propagate the correction through the spanning tree
  {
    std::unique_lock<std::mutex> lock(mMutexGBA);
    if (idx != mnFullBAIdx)
      return;

    if (!mbStopGBA) {
      mpLocalMapper->RequestStop();
      // Wait until Local Mapping has effectively stopped
      while (!mpLocalMapper->isStopped() && !mpLocalMapper->isFinished())
        usleep(1000);

      // Get Map Mutex
      std::unique_lock<std::mutex> lock(mpMap->mMutexMapUpdate);

      // Correct keyframes starting at first keyframe
      std::list<KeyFrame *> lpKFtoCheck(mpMap->mvpKeyFrameOrigins.begin(), mpMap->mvpKeyFrameOrigins.end());

      while (!lpKFtoCheck.empty()) {
        KeyFrame *pKF = lpKFtoCheck.front();
        const std::set<KeyFrame *> sChilds = pKF->GetChilds();
        cv::Mat Twc = pKF->GetPoseInverse();
        for (std::set<KeyFrame *>::const_iterator sit = sChilds.begin(); sit != sChilds.end(); sit++) {
          KeyFrame *pChild = *sit;
          if (pChild->mnBAGlobalForKF != nLoopKF) {
            cv::Mat Tchildc = pChild->GetPose() * Twc;
            pChild->mTcwGBA = Tchildc * pKF->mTcwGBA; //*Tcorc*pKF->mTcwGBA;
            pChild->mnBAGlobalForKF = nLoopKF;
          }
          lpKFtoCheck.push_back(pChild);
        }

        pKF->mTcwBefGBA = pKF->GetPose();
        pKF->SetPose(pKF->mTcwGBA);
        lpKFtoCheck.pop_front();
      }

      // Correct MapPoints
      for (MapPoint *pMP : mpMap->GetAllMapPoints()) {

        if (pMP->isBad())
          continue;

        if (pMP->mnBAGlobalForKF == nLoopKF) {
          // If optimized by Global BA, just update
          pMP->SetWorldPos(pMP->mPosGBA);
        } else {
          // Update according to the correction of its reference keyframe
          KeyFrame *pRefKF = pMP->GetReferenceKeyFrame();

          if (pRefKF->mnBAGlobalForKF != nLoopKF)
            continue;

          // Map to non-corrected camera
          cv::Mat Rcw = pRefKF->mTcwBefGBA.rowRange(0, 3).colRange(0, 3);
          cv::Mat tcw = pRefKF->mTcwBefGBA.rowRange(0, 3).col(3);
          cv::Mat Xc = Rcw * pMP->GetWorldPos() + tcw;

          // Backproject using corrected camera
          cv::Mat Twc = pRefKF->GetPoseInverse();
          cv::Mat Rwc = Twc.rowRange(0, 3).colRange(0, 3);
          cv::Mat twc = Twc.rowRange(0, 3).col(3);

          pMP->SetWorldPos(Rwc * Xc + twc);
        }
      }

      mpMap->InformNewBigChange();
      mpLocalMapper->Release();
    }

    mbFinishedGBA = true;
    mbRunningGBA = false;
  }
}

void LoopClosing::RequestFinish()
{
  std::unique_lock<std::mutex> lock(mMutexFinish);
  mbFinishRequested = true;
}

bool LoopClosing::CheckFinish()
{
  std::unique_lock<std::mutex> lock(mMutexFinish);
  return mbFinishRequested;
}

void LoopClosing::SetFinish()
{
  std::unique_lock<std::mutex> lock(mMutexFinish);
  mbFinished = true;
}

bool LoopClosing::isFinished()
{
  std::unique_lock<std::mutex> lock(mMutexFinish);
  return mbFinished;
}
}
