#include <glog/logging.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>

#include "vocabulary.pb.h"
#include <DBoW2/ScoringObject.h>
#include <ORBVocabulary.h>

#include <iostream>

namespace slam
{

void ORBVocabulary::loadFromTextFile(const std::string &filename)
{
  std::ifstream f(filename.c_str());
  CHECK(f.is_open()) << "Couldn't open " << filename;

  m_words.clear();
  m_nodes.clear();

  std::string s;
  std::getline(f, s);
  std::stringstream ss;
  ss << s;
  ss >> m_k;
  ss >> m_L;
  int n1, n2;
  ss >> n1;
  ss >> n2;

  bool isBad = (m_k < 0 || m_k > 20 || m_L < 1 || m_L > 10 || n1 < 0 || n1 > 5 || n2 < 0 || n2 > 3);
  CHECK(!isBad) << "Vocabulary loading failure: This is not a correct text file!" << std::endl;

  m_scoring = (DBoW2::ScoringType)n1;
  m_weighting = (DBoW2::WeightingType)n2;
  createScoringObject();

  // nodes
  int expected_nodes = (int)((pow((double)m_k, (double)m_L + 1) - 1) / (m_k - 1));
  m_nodes.reserve(expected_nodes);

  m_words.reserve(pow((double)m_k, (double)m_L + 1));

  m_nodes.resize(1);
  m_nodes[0].id = 0;
  while (!f.eof()) {
    std::string snode;
    std::getline(f, snode);
    std::stringstream ssnode;
    ssnode << snode;

    int nid = m_nodes.size();
    m_nodes.resize(m_nodes.size() + 1);
    m_nodes[nid].id = nid;

    int pid;
    ssnode >> pid;
    m_nodes[nid].parent = pid;
    m_nodes[pid].children.push_back(nid);

    int nIsLeaf;
    ssnode >> nIsLeaf;

    std::stringstream ssd;
    for (int iD = 0; iD < DBoW2::FORB::L; iD++) {
      std::string sElement;
      ssnode >> sElement;
      ssd << sElement << " ";
    }
    DBoW2::FORB::fromString(m_nodes[nid].descriptor, ssd.str());

    ssnode >> m_nodes[nid].weight;

    if (nIsLeaf > 0) {
      int wid = m_words.size();
      m_words.resize(wid + 1);

      m_nodes[nid].word_id = wid;
      m_words[wid] = &m_nodes[nid];
    } else {
      m_nodes[nid].children.reserve(m_k);
    }
  }
}

void ORBVocabulary::saveToTextFile(const std::string &filename) const
{
  std::fstream f;
  f.open(filename.c_str(), std::ios_base::out);
  f << m_k << " " << m_L << " "
    << " " << m_scoring << " " << m_weighting << std::endl;

  for (size_t i = 1; i < m_nodes.size(); i++) {
    const Node &node = m_nodes[i];

    f << node.parent << " ";
    if (node.isLeaf())
      f << 1 << " ";
    else
      f << 0 << " ";

    f << DBoW2::FORB::toString(node.descriptor) << " " << (double)node.weight << std::endl;
  }

  f.close();
}

void ORBVocabulary::loadFromProto(const std::string &filename)
{
  std::ifstream file(filename.c_str());
  CHECK(file.is_open()) << "Couldn't open " << filename;

  google::protobuf::io::IstreamInputStream raw_in(&file);
  google::protobuf::io::CodedInputStream coded_in(&raw_in);
  coded_in.SetTotalBytesLimit(std::numeric_limits<int>::max(), -1);

  proto::Vocabulary vocabulary_proto;
  CHECK(vocabulary_proto.ParseFromCodedStream(&coded_in));

  m_k = vocabulary_proto.k();
  m_L = vocabulary_proto.l();
  m_scoring = static_cast<DBoW2::ScoringType>(vocabulary_proto.scoring_type());
  m_weighting = static_cast<DBoW2::WeightingType>(vocabulary_proto.weighting_type());

  createScoringObject();

  // nodes
  m_nodes.resize(vocabulary_proto.nodes_size() + 1); // +1 to include root
  m_nodes[0].id = 0;

  for (const proto::Node &node : vocabulary_proto.nodes()) {
    const DBoW2::NodeId node_id = node.node_id();
    const DBoW2::NodeId parent_id = node.parent_id();

    m_nodes[node_id].id = node.node_id();
    m_nodes[node_id].parent = node.parent_id();
    m_nodes[node_id].weight = node.weight();

    m_nodes[parent_id].children.push_back(node_id);

    // For now only works with ORB.
    CHECK_EQ(node.node_descriptor().length(), DBoW2::FORB::L);
    m_nodes[node_id].descriptor = cv::Mat(1, DBoW2::FORB::L, CV_8U, const_cast<char *>(node.node_descriptor().c_str())).clone();
  }

  m_words.resize(vocabulary_proto.words_size());

  for (const proto::Word &word : vocabulary_proto.words()) {
    const DBoW2::WordId word_id = word.word_id();
    const DBoW2::NodeId node_id = word.node_id();

    m_nodes[node_id].word_id = word_id;
    m_words[word_id] = &m_nodes[node_id];
  }
}

void ORBVocabulary::saveToProto(const std::string &filename) const
{
  std::ofstream file(filename);
  CHECK(file.is_open()) << "Couldn't open " << filename;
  proto::Vocabulary vocabulary_proto;

  vocabulary_proto.set_k(m_k);
  vocabulary_proto.set_l(m_L);
  vocabulary_proto.set_scoring_type(m_scoring);
  vocabulary_proto.set_weighting_type(m_weighting);

  std::vector<DBoW2::NodeId> parents, children;
  std::vector<DBoW2::NodeId>::const_iterator pit;

  parents.push_back(0); // root

  while (!parents.empty()) {
    DBoW2::NodeId pid = parents.back();
    parents.pop_back();

    const Node &parent = m_nodes[pid];
    children = parent.children;

    for (pit = children.begin(); pit != children.end(); pit++) {
      const Node &child = m_nodes[*pit];

      proto::Node *node_proto = vocabulary_proto.add_nodes();
      CHECK_NOTNULL(node_proto);
      node_proto->set_node_id(child.id);
      node_proto->set_parent_id(pid);
      node_proto->set_weight(child.weight);

      // For now only works with BRIEF.
      char descriptor_array[DBoW2::FORB::L];
      CHECK_EQ(child.descriptor.rows, 1);
      CHECK_EQ(child.descriptor.cols, DBoW2::FORB::L);
      memcpy(descriptor_array, child.descriptor.data, DBoW2::FORB::L);
      node_proto->set_node_descriptor(std::string(descriptor_array, DBoW2::FORB::L));

      // add to parent list
      if (!child.isLeaf()) {
        parents.push_back(*pit);
      }
    }
  }

  typename std::vector<Node *>::const_iterator wit;
  for (wit = m_words.begin(); wit != m_words.end(); wit++) {
    DBoW2::WordId id = wit - m_words.begin();

    proto::Word *word_proto = vocabulary_proto.add_words();
    CHECK_NOTNULL(word_proto);
    word_proto->set_word_id(id);
    word_proto->set_node_id((*wit)->id);
  }

  CHECK(vocabulary_proto.SerializeToOstream(&file));
}
}
