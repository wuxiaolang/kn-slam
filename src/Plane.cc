/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdlib>
#include <mutex>
#include <opencv2/highgui.hpp>
#include <thread>

#include <DUtils/Random.h>
#include <Plane.h>

namespace slam
{

const float eps = 1e-4f;

cv::Mat ExpSO3(const float &x, const float &y, const float &z)
{
  cv::Mat I = cv::Mat::eye(3, 3, CV_32F);
  const float d2 = x * x + y * y + z * z;
  const float d = std::sqrt(d2); // Rotation angle
  // Compute the skew-symmetric matrix of the rotation vector [x,y,z]
  cv::Mat W = (cv::Mat_<float>(3, 3) << 0, -z, y, z, 0, -x, -y, x, 0);
  if (d < eps)
    return I + W + 0.5 * W * W;
  else
    return I + W * std::sin(d) / d + W * W * (1.0 - std::cos(d)) / d2;
}

cv::Mat ExpSO3(const cv::Mat &v)
{
  return ExpSO3(v.at<float>(0), v.at<float>(1), v.at<float>(2));
}

std::vector<MapPoint *> Plane::Detect(const std::vector<MapPoint *> &vMPs, const int iterations)
{
  // Retrieve 3D points
  std::vector<MapPoint *> vInlierMPs;
  std::vector<cv::Mat> vPoints;
  vPoints.reserve(vMPs.size());
  std::vector<MapPoint *> vPointMP;
  vPointMP.reserve(vMPs.size());

  for (MapPoint *pMP : vMPs) {
    if (pMP) {
      if (pMP->Observations() > 5) {
        vPoints.push_back(pMP->GetWorldPos());
        vPointMP.push_back(pMP);
      }
    }
  }

  const size_t N = vPoints.size();

  if (N < 50)
    return vInlierMPs;


  // Indices for minimum set selection
  std::vector<size_t> vAllIndices;
  std::vector<size_t> vAvailableIndices;
  for (size_t i = 0; i < N; i++) {
    vAllIndices.push_back(i);
  }

  float bestDist = 1e10;
  std::vector<float> bestvDist;

  // RANSAC
  for (int n = 0; n < iterations; n++) {
    vAvailableIndices = vAllIndices;

    cv::Mat A(3, 4, CV_32F);
    A.col(3) = cv::Mat::ones(3, 1, CV_32F);

    // Get min set of points
    for (short i = 0; i < 3; ++i) {
      int randi = DUtils::Random::RandomInt(0, vAvailableIndices.size() - 1);
      int idx = vAvailableIndices[randi];
      A.row(i).colRange(0, 3) = vPoints[idx].t();
      vAvailableIndices[randi] = vAvailableIndices.back();
      vAvailableIndices.pop_back();
    }

    cv::Mat u, w, vt;
    cv::SVDecomp(A, w, u, vt, cv::SVD::MODIFY_A | cv::SVD::FULL_UV);

    const float a = vt.at<float>(3, 0);
    const float b = vt.at<float>(3, 1);
    const float c = vt.at<float>(3, 2);
    const float d = vt.at<float>(3, 3);

    std::vector<float> vDistances(N, 0.0f);
    const float f = 1.0f / std::sqrt(a * a + b * b + c * c + d * d);
    for (size_t i = 0; i < N; i++) {
      vDistances[i] = std::fabs(vPoints[i].at<float>(0) * a + vPoints[i].at<float>(1) * b + vPoints[i].at<float>(2) * c + d) * f;
    }

    std::vector<float> vSorted = vDistances;
    std::sort(vSorted.begin(), vSorted.end());
    size_t nth = N / 5 > 20 ? N / 5 : 20;
    const float medianDist = vSorted[nth];
    if (medianDist < bestDist) {
      bestDist = medianDist;
      bestvDist = vDistances;
    }
  }

  // Compute threshold inlier/outlier
  const float th = 1.4f * bestDist;
  for (size_t i = 0; i < N; ++i) {
    if (bestvDist[i] < th) {
      vInlierMPs.push_back(vPointMP[i]);
    }
  }

  return vInlierMPs;
}

Plane::Plane(const std::vector<MapPoint *> &vMPs, const cv::Mat &Tcw)
    : mvMPs(vMPs)
    , mTcw(Tcw.clone())
{
  rang = -3.14f / 2 + (rand() / RAND_MAX) * 3.14f;
  Recompute();
}

void Plane::Recompute()
{
  const size_t N = mvMPs.size();

  // Recompute plane with all points
  cv::Mat A(N, 4, CV_32F);
  A.col(3) = cv::Mat::ones(N, 1, CV_32F);

  o = cv::Mat::zeros(3, 1, CV_32F);

  int nPoints = 0;
  for (MapPoint *pMP : mvMPs) {
    if (!pMP->isBad()) {
      cv::Mat Xw = pMP->GetWorldPos();
      o += Xw;
      A.row(nPoints).colRange(0, 3) = Xw.t();
      nPoints++;
    }
  }

  A.resize(nPoints);

  cv::Mat u, w, vt;
  cv::SVDecomp(A, w, u, vt, cv::SVD::MODIFY_A | cv::SVD::FULL_UV);

  float a = vt.at<float>(3, 0);
  float b = vt.at<float>(3, 1);
  float c = vt.at<float>(3, 2);

  o = o * (1.0 / nPoints);
  const float f = 1.0f / std::sqrt(a * a + b * b + c * c);

  // Compute XC just the first time
  if (XC.empty()) {
    cv::Mat Oc = -mTcw.colRange(0, 3).rowRange(0, 3).t() * mTcw.rowRange(0, 3).col(3);
    XC = Oc - o;
  }

  if ((XC.at<float>(0) * a + XC.at<float>(1) * b + XC.at<float>(2) * c) > 0) {
    a = -a;
    b = -b;
    c = -c;
  }

  n = (cv::Mat_<float>(3, 1) << a * f, b * f, c * f);
  ComputeTpw();
}

Plane::Plane(const float &nx, const float &ny, const float &nz, const float &ox, const float &oy, const float &oz)
{
  o = (cv::Mat_<float>(3, 1) << ox, oy, oz);
  n = (cv::Mat_<float>(3, 1) << nx, ny, nz);
  ComputeTpw();
}

void Plane::ComputeTpw()
{
  cv::Mat up = (cv::Mat_<float>(3, 1) << 0.0f, 1.0f, 0.0f);
  cv::Mat v = up.cross(n);
  const float s = cv::norm(v);
  const float c = up.dot(n);
  const float a = atan2(s, c);
  Tpw = cv::Mat::eye(4, 4, CV_32F);
  const float rang = -3.14f / 2 + (rand() / RAND_MAX) * 3.14f;
  Tpw.rowRange(0, 3).colRange(0, 3) = ExpSO3(v * a / s) * ExpSO3(up * rang);
  o.copyTo(Tpw.col(3).rowRange(0, 3));
}
}
