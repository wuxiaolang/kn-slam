/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iomanip>
#include <thread>

#include <Converter.h>
#include <FrameDrawer.h>
#include <LocalMapping.h>
#include <LoopClosing.h>
#include <Settings.h>
#include <System.h>
#include <Tracking.h>
#include <global.h>

// definition of global performance monitor
#ifdef SLAM_TRACE
slam::PerformanceMonitor *g_permon = NULL;
#endif

namespace slam
{

System::System(
    const std::string &strVocFile,
    const std::string &strSettings,
    const eSensor sensor)
    : mSensor(sensor)
    , mbReset(false)
    , mbActivateLocalizationMode(false)
    , mbDeactivateLocalizationMode(false)
{

#ifdef SLAM_TRACE
  // Initialize the Performance Monitor
  g_permon = new slam::PerformanceMonitor();
  g_permon->addTimer("vocabulary_loading");
  g_permon->init("kn", "/tmp");
#endif

  // Load settings
  Settings::getInstance().loadFrom(strSettings);

  // Load vocabulary tree
  START_TIMER("vocabulary_loading");
  mpVocabulary = new ORBVocabulary();
  mpVocabulary->loadFromProto(strVocFile);
  PRINT_TIMER("vocabulary_loading");

  // Create KeyFrame Database
  mpKeyFrameDatabase = new KeyFrameDatabase(*mpVocabulary);

  // Create the Map
  mpMap = new Map();

  // Initialize the Tracking thread
  mpTracker = new Tracking(this, mpVocabulary, mpMap, mpKeyFrameDatabase, mSensor);

  // Initialize the Local Mapping thread and launch
  mpLocalMapper = new LocalMapping(mpMap, mSensor == MONOCULAR);
  mptLocalMapping = new std::thread(&LocalMapping::Run, mpLocalMapper);

  // Initialize the Loop Closing thread and launch
  mpLoopCloser = new LoopClosing(mpMap, mpKeyFrameDatabase, mpVocabulary, mSensor != MONOCULAR);
  mptLoopClosing = new std::thread(&LoopClosing::Run, mpLoopCloser);

  // Set pointers between threads
  mpTracker->SetLocalMapper(mpLocalMapper);
  mpTracker->SetLoopClosing(mpLoopCloser);

  mpLocalMapper->SetTracker(mpTracker);
  mpLocalMapper->SetLoopCloser(mpLoopCloser);

  mpLoopCloser->SetTracker(mpTracker);
  mpLoopCloser->SetLocalMapper(mpLocalMapper);
}

cv::Mat System::TrackStereo(const cv::Mat &imLeft, const cv::Mat &imRight, const double &timestamp)
{
  if (mSensor != STEREO) {
    ERROR_STREAM("TrackStereo", "Input sensor not set to STEREO")
    exit(-1);
  }

  // Check mode change
  {
    std::unique_lock<std::mutex> lock(mMutexMode);
    if (mbActivateLocalizationMode) {
      mpLocalMapper->RequestStop();

      // Wait until Local Mapping has effectively stopped
      while (!mpLocalMapper->isStopped()) {
        usleep(1000);
      }

      mpTracker->InformOnlyTracking(true);
      mbActivateLocalizationMode = false;
    }
    if (mbDeactivateLocalizationMode) {
      mpTracker->InformOnlyTracking(false);
      mpLocalMapper->Release();
      mbDeactivateLocalizationMode = false;
    }
  }

  // Check reset
  {
    std::unique_lock<std::mutex> lock(mMutexReset);
    if (mbReset) {
      mpTracker->Reset();
      mbReset = false;
    }
  }

  cv::Mat Tcw = mpTracker->GrabImageStereo(imLeft, imRight, timestamp);

  std::unique_lock<std::mutex> lock2(mMutexState);
  mTrackingState = mpTracker->mState;
  mTrackedMapPoints = mpTracker->mCurrentFrame.mvpMapPoints;
  mTrackedKeyPointsUn = mpTracker->mCurrentFrame.mvKeysUn;
  return Tcw;
}

cv::Mat System::TrackRGBD(const cv::Mat &im, const cv::Mat &depthmap, const double &timestamp)
{
  if (mSensor != RGBD) {
    ERROR_STREAM("TrackRGBD", "Input sensor not set to RGBD")
    exit(-1);
  }

  // Check mode change
  {
    std::unique_lock<std::mutex> lock(mMutexMode);
    if (mbActivateLocalizationMode) {
      mpLocalMapper->RequestStop();

      // Wait until Local Mapping has effectively stopped
      while (!mpLocalMapper->isStopped()) {
        usleep(1000);
      }

      mpTracker->InformOnlyTracking(true);
      mbActivateLocalizationMode = false;
    }
    if (mbDeactivateLocalizationMode) {
      mpTracker->InformOnlyTracking(false);
      mpLocalMapper->Release();
      mbDeactivateLocalizationMode = false;
    }
  }

  // Check reset
  {
    std::unique_lock<std::mutex> lock(mMutexReset);
    if (mbReset) {
      mpTracker->Reset();
      mbReset = false;
    }
  }

  cv::Mat Tcw = mpTracker->GrabImageRGBD(im, depthmap, timestamp);

  std::unique_lock<std::mutex> lock2(mMutexState);
  mTrackingState = mpTracker->mState;
  mTrackedMapPoints = mpTracker->mCurrentFrame.mvpMapPoints;
  mTrackedKeyPointsUn = mpTracker->mCurrentFrame.mvKeysUn;
  return Tcw;
}

cv::Mat System::TrackMonocular(const cv::Mat &im, const double &timestamp)
{
  if (mSensor != MONOCULAR) {
    ERROR_STREAM("TrackMonocular", "Input sensor not set to Monocular")
    exit(-1);
  }

  // Check mode change
  {
    std::unique_lock<std::mutex> lock(mMutexMode);
    if (mbActivateLocalizationMode) {
      mpLocalMapper->RequestStop();

      // Wait until Local Mapping has effectively stopped
      while (!mpLocalMapper->isStopped()) {
        usleep(1000);
      }

      mpTracker->InformOnlyTracking(true);
      mbActivateLocalizationMode = false;
    }
    if (mbDeactivateLocalizationMode) {
      mpTracker->InformOnlyTracking(false);
      mpLocalMapper->Release();
      mbDeactivateLocalizationMode = false;
    }
  }

  // Check reset
  {
    std::unique_lock<std::mutex> lock(mMutexReset);
    if (mbReset) {
      mpTracker->Reset();
      mbReset = false;
    }
  }

  cv::Mat Tcw = mpTracker->GrabImageMonocular(im, timestamp);

  std::unique_lock<std::mutex> lock2(mMutexState);
  mTrackingState = mpTracker->mState;
  mTrackedMapPoints = mpTracker->mCurrentFrame.mvpMapPoints;
  mTrackedKeyPointsUn = mpTracker->mCurrentFrame.mvKeysUn;

  return Tcw;
}

void System::ActivateLocalizationMode()
{
  std::unique_lock<std::mutex> lock(mMutexMode);
  mbActivateLocalizationMode = true;
}

void System::DeactivateLocalizationMode()
{
  std::unique_lock<std::mutex> lock(mMutexMode);
  mbDeactivateLocalizationMode = true;
}

bool System::MapChanged()
{
  static int n = 0;
  int curn = mpMap->GetLastBigChangeIdx();
  if (n < curn) {
    n = curn;
    return true;
  } else
    return false;
}

void System::Reset()
{
  std::unique_lock<std::mutex> lock(mMutexReset);
  mbReset = true;
}

void System::Shutdown()
{
  mpLocalMapper->RequestFinish();
  mpLoopCloser->RequestFinish();

  // Wait until all thread have effectively stopped
  while (!mpLocalMapper->isFinished() || !mpLoopCloser->isFinished() || mpLoopCloser->isRunningGBA()) {
    usleep(5000);
  }
}

void System::SaveTrajectoryTUM(const std::string &filename)
{
  if (mSensor == MONOCULAR) {
    std::cerr << "ERROR: SaveTrajectoryTUM cannot be used for monocular." << std::endl;
    return;
  }

  std::vector<KeyFrame *> vpKFs = mpMap->GetAllKeyFrames();
  std::sort(vpKFs.begin(), vpKFs.end(), KeyFrame::lId);

  // Transform all keyframes so that the first keyframe is at the origin.
  // After a loop closure the first keyframe might not be at the origin.
  cv::Mat Two = vpKFs[0]->GetPoseInverse();

  std::ofstream f;
  f.open(filename.c_str());
  f << std::fixed;

  // Frame pose is stored relative to its reference keyframe (which is optimized by BA and pose graph).
  // We need to get first the keyframe pose and then concatenate the relative transformation.
  // Frames not localized (tracking failure) are not saved.

  // For each frame we have a reference keyframe (lRit), the timestamp (lT) and a flag
  // which is true when tracking failed (lbL).
  std::list<KeyFrame *>::iterator lRit = mpTracker->mlpReferences.begin();
  std::list<double>::iterator lT = mpTracker->mlFrameTimes.begin();
  std::list<bool>::iterator lbL = mpTracker->mlbLost.begin();
  for (std::list<cv::Mat>::iterator lit = mpTracker->mlRelativeFramePoses.begin(),
                                    lend = mpTracker->mlRelativeFramePoses.end();
       lit != lend; lit++, lRit++, lT++, lbL++) {
    if (*lbL)
      continue;

    KeyFrame *pKF = *lRit;

    cv::Mat Trw = cv::Mat::eye(4, 4, CV_32F);

    // If the reference keyframe was culled, traverse the spanning tree to get a suitable keyframe.
    while (pKF->isBad()) {
      Trw = Trw * pKF->mTcp;
      pKF = pKF->GetParent();
    }

    Trw = Trw * pKF->GetPose() * Two;

    cv::Mat Tcw = (*lit) * Trw;
    cv::Mat Rwc = Tcw.rowRange(0, 3).colRange(0, 3).t();
    cv::Mat twc = -Rwc * Tcw.rowRange(0, 3).col(3);

    std::vector<float> q = Converter::toQuaternion(Rwc);

    f << std::setprecision(6) << *lT << " " << std::setprecision(9) << twc.at<float>(0) << " " << twc.at<float>(1) << " " << twc.at<float>(2) << " " << q[0] << " " << q[1] << " " << q[2] << " " << q[3] << std::endl;
  }
  f.close();
}

void System::SaveKeyFrameTrajectoryTUM(const std::string &filename)
{
  std::vector<KeyFrame *> vpKFs = mpMap->GetAllKeyFrames();
  std::sort(vpKFs.begin(), vpKFs.end(), KeyFrame::lId);

  std::ofstream f(filename.c_str());
  f << std::fixed;

  for (KeyFrame *pKF : vpKFs) {
    if (pKF->isBad())
      continue;

    cv::Mat R = pKF->GetRotation().t();
    std::vector<float> q = Converter::toQuaternion(R);
    cv::Mat t = pKF->GetCameraCenter();

    f << std::setprecision(6) << pKF->mTimeStamp << std::setprecision(7) << " "
      << t.at<float>(0) << " " << t.at<float>(1) << " " << t.at<float>(2)
      << " " << q[0] << " " << q[1] << " " << q[2] << " " << q[3] << std::endl;
  }

  f.close();
  INFO_STREAM("SYSTEM", "# KeyFrames: " << vpKFs.size())
}

void System::SaveTrajectoryKITTI(const std::string &filename)
{
  if (mSensor == MONOCULAR) {
    ERROR_STREAM("SYSTEM", "ERROR: SaveTrajectoryKITTI cannot be used for monocular")
    return;
  }

  std::vector<KeyFrame *> vpKFs = mpMap->GetAllKeyFrames();
  std::sort(vpKFs.begin(), vpKFs.end(), KeyFrame::lId);

  // Transform all keyframes so that the first keyframe is at the origin.
  // After a loop closure the first keyframe might not be at the origin.
  cv::Mat Two = vpKFs[0]->GetPoseInverse();

  std::ofstream f;
  f.open(filename.c_str());
  f << std::fixed;

  // Frame pose is stored relative to its reference keyframe (which is optimized by BA and pose graph).
  // We need to get first the keyframe pose and then concatenate the relative transformation.
  // Frames not localized (tracking failure) are not saved.

  // For each frame we have a reference keyframe (lRit), the timestamp (lT) and a flag
  // which is true when tracking failed (lbL).
  std::list<KeyFrame *>::iterator lRit = mpTracker->mlpReferences.begin();
  std::list<double>::iterator lT = mpTracker->mlFrameTimes.begin();
  for (std::list<cv::Mat>::iterator lit = mpTracker->mlRelativeFramePoses.begin(), lend = mpTracker->mlRelativeFramePoses.end(); lit != lend; lit++, lRit++, lT++) {
    KeyFrame *pKF = *lRit;

    cv::Mat Trw = cv::Mat::eye(4, 4, CV_32F);

    while (pKF->isBad()) {
      //  cout << "bad parent" << endl;
      Trw = Trw * pKF->mTcp;
      pKF = pKF->GetParent();
    }

    Trw = Trw * pKF->GetPose() * Two;

    cv::Mat Tcw = (*lit) * Trw;
    cv::Mat Rwc = Tcw.rowRange(0, 3).colRange(0, 3).t();
    cv::Mat twc = -Rwc * Tcw.rowRange(0, 3).col(3);

    f << std::setprecision(9) << Rwc.at<float>(0, 0) << " " << Rwc.at<float>(0, 1) << " " << Rwc.at<float>(0, 2) << " "
      << twc.at<float>(0) << " " << Rwc.at<float>(1, 0) << " " << Rwc.at<float>(1, 1) << " " << Rwc.at<float>(1, 2) << " "
      << twc.at<float>(1) << " " << Rwc.at<float>(2, 0) << " " << Rwc.at<float>(2, 1) << " " << Rwc.at<float>(2, 2) << " "
      << twc.at<float>(2) << std::endl;
  }
  f.close();
}

int System::GetTrackingState()
{
  std::unique_lock<std::mutex> lock(mMutexState);
  return mTrackingState;
}

std::vector<MapPoint *> System::GetTrackedMapPoints()
{
  std::unique_lock<std::mutex> lock(mMutexState);
  return mTrackedMapPoints;
}

std::vector<cv::KeyPoint> System::GetTrackedKeyPointsUn()
{
  std::unique_lock<std::mutex> lock(mMutexState);
  return mTrackedKeyPointsUn;
}

Map *System::getMap() const
{
  return mpMap;
}

Tracking *System::getTracker() const
{
  return mpTracker;
}

void System::SaveEdgeHistogram(const std::string &filename)
{
  std::vector<uint32_t> vEdgeHistogram;
  vEdgeHistogram.reserve(255);
  vEdgeHistogram.assign(255, 0);
  uint64_t nTotal = 0;
  for (auto pKF : mpMap->GetAllKeyFrames()) {
    if (pKF->isBad())
      continue;
    auto vCovisibles = pKF->GetCovisibleKeyFrames();
    auto vWeights = pKF->GetCovisibilityWeights();
    for (uint32_t i = 0; i < vCovisibles.size(); ++i) {
      if (!vCovisibles[i]->isBad()) {
        nTotal++;
        uint32_t w = vWeights[i];
        if (w < 255)
          vEdgeHistogram[w]++;
      }
    }
  }

  // Print the weight at a new line (required by numpy.read)
  std::ofstream f;
  f.open(filename.c_str());
  for (auto &edge_count : vEdgeHistogram)
    f << edge_count << std::endl;
  f.close();

  INFO_STREAM("SYSTEM", "# Edges: " << nTotal)
}

void System::SaveGraphDOT(const std::string &filename)
{
  std::vector<KeyFrame *> vpKFs = mpMap->GetAllKeyFrames();
  std::ofstream f(filename.c_str());

  // Write header
  f << "digraph poses {" << std::endl;

  for (KeyFrame *pKF : vpKFs) {
    if (pKF->isBad())
      continue;

    auto vCovisibles = pKF->GetCovisibleKeyFrames();
    auto vWeights = pKF->GetCovisibilityWeights();
    for (uint32_t i = 0; i < vCovisibles.size(); ++i) {
      if (!vCovisibles[i]->isBad()) {
        f << '\t' << pKF->mnId << " -> " << vCovisibles[i]->mnId << "[weight=" << vWeights[i] << "];" << std::endl;
      }
    }
  }

  f << "}";
  f.close();

  INFO_STREAM("SYSTEM", "Connectivity Graph saved at " << filename)
}

void System::SaveMap(const std::string &filename)
{
  std::ofstream f;
  f.open(filename.c_str());
  f << std::fixed;

  f << "ply" << std::endl;
  f << "format ascii 1.0" << std::endl;
  f << "element vertex " << mpMap->PointsCount() << std::endl;
  f << "property float x" << std::endl;
  f << "property float y" << std::endl;
  f << "property float z" << std::endl;
  f << "property float red" << std::endl;
  f << "property float green" << std::endl;
  f << "property float blue" << std::endl;
  f << "end_header" << std::endl;

  for (MapPoint *mp : mpMap->GetAllMapPoints()) {
    cv::Mat p = mp->GetWorldPos();
    cv::Vec3f c = mp->GetRGB() / 255.0;
    f << p.at<float>(0) << ' ' << p.at<float>(1) << ' ' << p.at<float>(2) << ' ' << c[0] << ' ' << c[1] << ' ' << c[2] << std::endl;
  }
  f.close();
}

} // namespace slam
