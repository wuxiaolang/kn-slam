#include <chrono>
#include <fstream>
#include <iostream>
#include <thread>

#include <opencv2/highgui.hpp>

#include <GLViewer.h>
#include <TUMLoader.h>
#include <global.h>

namespace slam
{

TUMLoader::TUMLoader(const std::string &path, const std::shared_ptr<System> &system)
    : Loader(system)
    , msPath(path)
{
}

void TUMLoader::LoadImages(
    const std::string &strFile,
    std::vector<std::string> &vstrImageFilenames,
    std::vector<double> &vTimestamps)
{
  std::ifstream f;
  f.open(strFile.c_str());

  while (!f.eof()) {
    std::string s;
    std::getline(f, s);
    if (s.empty())
      continue;
    std::istringstream ss(s);
    double t;
    ss >> t;
    if (ss.fail())
      continue;
    vTimestamps.push_back(t);
    std::string sRGB;
    ss >> sRGB;
    vstrImageFilenames.push_back(sRGB);
  }
}

void TUMLoader::run()
{
  INFO_STREAM("TUMloader", "Loading images from " << msPath)

  // Retrieve paths to images
  std::vector<std::string> vstrImageFilenames;
  std::vector<double> vTimestamps;
  std::string strFile = msPath + "/rgb.txt";
  LoadImages(strFile, vstrImageFilenames, vTimestamps);

  // Vector for tracking time statistics
  std::vector<double> vTimesTrack;
  size_t nImages = vstrImageFilenames.size();
  vTimesTrack.resize(nImages);

  // Main loop
  for (size_t ni = 0; ni < nImages; ni++) {
    std::string fn = msPath + "/" + vstrImageFilenames[ni];
    cv::Mat im = cv::imread(fn, CV_LOAD_IMAGE_UNCHANGED);
    double tframe = vTimestamps[ni];

    if (im.empty()) {
      ERROR_STREAM("TUMLoader", "Failed to load image at: " << fn)
      return;
    }

    std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();

    // Pass the image to the SLAM system
    mpSystem->TrackMonocular(im, vTimestamps[ni]);

    std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
    double ttrack = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();
    vTimesTrack[ni] = ttrack;

    // Wait to load the next frame
    double T = 0;
    if (ni < nImages - 1)
      T = vTimestamps[ni + 1] - tframe;
    else if (ni > 0)
      T = tframe - vTimestamps[ni - 1];

    if (ttrack < T) {
      long ms = static_cast<long>((T - ttrack) * 1000);
      std::this_thread::sleep_for(std::chrono::milliseconds(ms));
    }

    if (stopRequested())
      break;
  }

  // Print some useful tracking stats
  std::sort(vTimesTrack.begin(), vTimesTrack.end());
  double totalTime = cv::sum(vTimesTrack)[0];
  INFO_STREAM("SYSTEM", "images: " << nImages)
  INFO_STREAM("SYSTEM", "total : " << totalTime)
  INFO_STREAM("SYSTEM", "median: " << vTimesTrack[vTimesTrack.size() / 2])
  INFO_STREAM("SYSTEM", "mean  : " << totalTime / vTimesTrack.size())

  // Notify viewer to finish
  if (mpViewer)
    mpViewer->requestFinish();
}
}
