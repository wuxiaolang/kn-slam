/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <ros/ros.h>

#include "System.h"

#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

class ImageGrabber
{
  public:
  ImageGrabber(slam::System *pSLAM)
      : mpSLAM(pSLAM)
  {
  }

  void GrabImage(const sensor_msgs::ImageConstPtr &msg);

  slam::System *mpSLAM;
  std::vector<double> vTimesTrack;
};


void ImageGrabber::GrabImage(const sensor_msgs::ImageConstPtr &msg)
{
  cv_bridge::CvImageConstPtr cv_ptr;
  try {
    cv_ptr = cv_bridge::toCvShare(msg, "bgr8");
  } catch (cv_bridge::Exception &e) {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }

  std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
  mpSLAM->TrackMonocular(cv_ptr->image, cv_ptr->header.stamp.toSec());
  std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();

  double ttrack = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();
  vTimesTrack.push_back(ttrack);
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "mono");
  ros::start();

  if (argc != 3) {
    ROS_ERROR("Usage: rosrun orb_slam mono path_to_vocabulary path_to_settings");
    ros::shutdown();
    return 1;
  }

  // Create SLAM system. It initializes all system threads and gets ready to process frames.
  slam::System system(argv[1], argv[2], slam::System::MONOCULAR, false);

  ImageGrabber igb(&system);
  ros::NodeHandle nh;
  image_transport::ImageTransport it(nh);
  image_transport::Subscriber sub = it.subscribe("/camera/image_raw", 10, &ImageGrabber::GrabImage, &igb);

  ROS_INFO("Waiting for image messages from topic %s", ros::names::remap("image").c_str());

  ros::spin();

  // Stop all threads
  system.Shutdown();

  // Print time stats
  auto &times = igb.vTimesTrack;
  std::sort(times.begin(), times.end());
  double totalTime = cv::sum(times)[0];
  std::cout << "total : " << totalTime << std::endl;
  std::cout << "median: " << times[times.size() / 2] << std::endl;
  std::cout << "mean  : " << totalTime / times.size() << std::endl;

  // Save kf trajectory, histogram and map
  system.SaveKeyFrameTrajectoryTUM("/tmp/trajectory.txt");
  system.SaveEdgeHistogram("/tmp/histogram.txt");
  system.SaveMap("/tmp/map.ply");

  ros::shutdown();
}
